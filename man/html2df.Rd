\name{html2df}
\alias{html2df}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{Read a HTML notebook file downloaded from cbdx.cz}
\description{
  This function reads a CB notebook saved in the HTML format 
  produced by the notebook databank website. It returns a dataframe with 
  some .c6v columns (date and time, called station QTH) parsed into
  meaningful atomic data items (day, time, geographical location).
}
\usage{
html2df(file, mode)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{file}{
    html2df(FILENAME, MODE)
}
}
\details{
  FILENAME	A string with the full filename to be read in (path/filename)
  MODE		Character coding conversion to be carried out by iconv(). Currently one of ASCII//TRANSLIT (default) or (UTF8). This argument is passed on to the iconv() "to=" argument.
}
\value{
  dataframe
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
  Matej Lexa
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.
htmlfile <- system.file("/extdata","xdenik3363.html",package="rcl6")
htmldata <- html2df(htmlfile)
head(htmldata)

## The function is currently defined as
function (file) 
{
    df <- read.table(file, sep = ";", header = F, skip = 1)
    colnames(df) <- c("locator_out", "id", "date_time", "report_out", 
        "qth_in", "report_in", "locator_in", "dist", "note")
    return(df)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
