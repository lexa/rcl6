plot_race <- function(df,col="black", newcanvas=T) {
  plot_station(df, col="blue", newcanvas=newcanvas)
  plot_locator(df$locator_in, col=col, newcanvas=F)
  for(i in 1:length(df$id)) {
    plot_line(df[i,c(1,7)],col=col, newcanvas=F)
  }
}

